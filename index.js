// index.js

var express = require("express");
var mongoose   = require("mongoose");
var bodyParser = require("body-parser");
var methodOverride = require("method-override");
var app = express();

mongoose.connect('mongodb://localhost/bbs_test');
var db = mongoose.connection;
db.once("open", function(){
    console.log("DB Connected");
});
db.on("error", function(err){
    console.log("DB ERROR : ", err);
});

//other settings
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(methodOverride("_method"));

//Routes
app.use("/", require("./routes/home")); // 사용자가 접속하면 ./routes/home.js를 실행
app.use("/posts", require("./routes/posts")); // 

//Port Setting
app.listen(8000, function(){
    console.log("server On!");
});